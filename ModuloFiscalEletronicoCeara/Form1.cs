﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Globalization;
using System.Threading;

namespace ModuloFiscalEletronicoCeara
{
    public partial class Form1 : Form
    {
        //Construtor.
        public Form1()
        {
            InitializeComponent();
        }

        String assinaturaSoftwareHouse, cnpjContribuinte, cnpjSoftwareHouse;
        int numeroSessao, identificador, codigoAtivacao, inscricaoEstadual;
        int i = 0;
        Random numeroAleatorio = new Random();

        // GroupBox das Funções.
        private void groupBox1_Enter(object sender, EventArgs e)
        {
        }
        // Form Principal.
        private void Form1_Load(object sender, EventArgs e)
        {
        }
        // Botão de Seleção da Pasta Input.
        private void button14_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog selectinput = new FolderBrowserDialog();
            selectinput.ShowDialog();
            textBox6.Text = selectinput.SelectedPath;
        }
        // Botão de Seleção da Pasta Output.
        private void button15_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog selectoutput = new FolderBrowserDialog();
            selectoutput.ShowDialog();
            textBox7.Text = selectoutput.SelectedPath;
        }
        // TextBox Output. 
        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }



        // Consultar MFE
        private void button1_Click(object sender, EventArgs e)
        {
            // Criando Número de Sessão.
            numeroSessao = numeroAleatorio.Next(000000, 999999);
            // Criando Identificador.
            identificador = numeroAleatorio.Next(0000000, 9999999);
            // XML.
            String xmlConsultarMfe = "<Integrador><Identificador>" +
                "<Valor>" + identificador + "</Valor></Identificador><Componente Nome=\"MF-e\" >" +
                "<Metodo Nome=\"ConsultarMFe\" ><Parametros><Parametro><Nome>numeroSessao</Nome>" +
                "<Valor>" + numeroSessao + "</Valor></Parametro></Parametros></Metodo>" +
                "</Componente></Integrador>";
            Write(xmlConsultarMfe);
            Read(1);
        }
        // Estado Operacional.
        private void button2_Click(object sender, EventArgs e)
        {
            // Criando Número de Sessão.
            numeroSessao = numeroAleatorio.Next(000000, 999999);
            // Criando Identificador.
            identificador = numeroAleatorio.Next(0000000, 9999999);
            // Código de Ativação.
            codigoAtivacao = Convert.ToInt32(textBox1.Text);
            // XML.
            String xmlEstadoOperacional = "<Integrador><Identificador><Valor>"+identificador+"</Valor>" +
                "</Identificador><Componente Nome=\"MF-e\"><Metodo Nome=\"ConsultarStatusOperacional\">" +
                "<Parametros><Parametro><Nome>numeroSessao</Nome><Valor>"+numeroSessao+"</Valor></Parametro>" +
                "<Parametro><Nome>codigoDeAtivacao</Nome><Valor>"+codigoAtivacao+"</Valor></Parametro></Parametros>" +
                "</Metodo></Componente></Integrador>";
            Write(xmlEstadoOperacional);
            Read(2);
        }
        // Extrair LOG.
        private void button3_Click(object sender, EventArgs e)
        {
            // Criando Número de Sessão.
            numeroSessao = numeroAleatorio.Next(000000, 999999);
            // Criando Identificador.
            identificador = numeroAleatorio.Next(0000000, 9999999);
            // Código de Ativação.
            codigoAtivacao = Convert.ToInt32(textBox1.Text);
            // XML
            String xmlExtrairLog = "<Integrador><Identificador><Valor>"+identificador+"</Valor>" +
                "</Identificador><Componente Nome=\"MF-e\"><Metodo Nome=\"ExtrairLogs\"><Parametros>" +
                "<Parametro><Nome>numeroSessao</Nome><Valor>"+numeroSessao+"</Valor></Parametro>" +
                "<Parametro><Nome>codigoDeAtivacao</Nome><Valor>"+codigoAtivacao+"</Valor></Parametro>" +
                "</Parametros></Metodo></Componente></Integrador>";
            Write(xmlExtrairLog);
            Read(3);
        }
        // Configurar Rede
        private void button4_Click(object sender, EventArgs e)
        {
        }
        // Ativar MFE.
        private void button5_Click(object sender, EventArgs e)
        {
            // Criando Número de Sessão.
            numeroSessao = numeroAleatorio.Next(000000, 999999);
            // Criando Identificador.
            identificador = numeroAleatorio.Next(0000000, 9999999);
            // Código de Ativação.
            codigoAtivacao = Convert.ToInt32(textBox1.Text);
            // CNPJ Contribuinte
            cnpjContribuinte = textBox2.Text;
            // XML
            String xmlAtivarMfe = "<Integrador><Identificador><Valor>" + identificador + "</Valor>" +
                "</Identificador><Componente Nome=\"MF-e\"><Metodo Nome=\"AtivarSAT\"><Parametros>" +
                "<Parametro><Nome>numeroSessao</Nome><Valor>" + numeroSessao + "</Valor></Parametro><Parametro>" +
                "<Nome>subComando</Nome><Valor>01</Valor></Parametro><Parametro><Nome>codigoDeAtivacao</Nome>" +
                "<Valor>" + codigoAtivacao + "</Valor></Parametro><Parametro><Nome>cnpj</Nome>" +
                "<Valor>" + cnpjContribuinte + "</Valor></Parametro><Parametro><Nome>cUf</Nome><Valor>23</Valor>" +
                "</Parametro></Parametros></Metodo></Componente></Integrador>";
            Write(xmlAtivarMfe);
            Read(5);
        }
        // Vincular MFE.
        private void button6_Click(object sender, EventArgs e)
        {
            // Criando Número de Sessão.
            numeroSessao = numeroAleatorio.Next(000000, 999999);
            // Criando Identificador.
            identificador = numeroAleatorio.Next(0000000, 9999999);
            // Código de Ativação.
            codigoAtivacao = Convert.ToInt32(textBox1.Text);
            // CNPJ Contribuinte.
            cnpjContribuinte = textBox2.Text;
            // CNPJ Software House.
            cnpjSoftwareHouse = textBox3.Text;
            // Assinatura SH
            assinaturaSoftwareHouse = textBox3.Text;
            String xmlVincularAC = "<Integrador><Identificador><Valor>" + identificador + "</Valor>" +
                "</Identificador><Componente Nome=\"MF-e\"><Metodo Nome=\"AssociarAssinatura\">" +
                "<Parametros><Parametro><Nome>numeroSessao</Nome><Valor>" + numeroSessao + "</Valor>" +
                "</Parametro><Parametro><Nome>codigoDeAtivacao</Nome><Valor>" + codigoAtivacao + "</Valor>" +
                "</Parametro><Parametro><Nome>cnpjValue</Nome><Valor>" + cnpjSoftwareHouse + "</Valor>" +
                "</Parametro><Parametro><Nome>assinaturaCNPJs</Nome><Valor>" + assinaturaSoftwareHouse + "</Valor>" +
                "</Parametro></Parametros></Metodo></Componente></Integrador>";
            Write(xmlVincularAC);
            Read(6);
        }
        // Trocar Código.
        private void button7_Click(object sender, EventArgs e)
        {

        }
        // Bloquear MFE.
        private void button8_Click(object sender, EventArgs e)
        {
            // Criando Número de Sessão.
            numeroSessao = numeroAleatorio.Next(000000, 999999);
            // Criando Identificador.
            identificador = numeroAleatorio.Next(0000000, 9999999);
            // Código de Ativação.
            codigoAtivacao = Convert.ToInt32(textBox1.Text);
            String xmlBloquearMfe = "<Integrador><Identificador><Valor>"+identificador+"</Valor>" +
                "</Identificador><Componente Nome=\"MF-e\"><Metodo Nome=\"BloquearSAT\"><Parametros>" +
                "<Parametro><Nome>numeroSessao</Nome><Valor>"+numeroSessao+"</Valor></Parametro><Parametro>" +
                "<Nome>codigoDeAtivacao</Nome><Valor>"+codigoAtivacao+"</Valor></Parametro></Parametros>" +
                "</Metodo></Componente></Integrador>";
            Write(xmlBloquearMfe);
            Read(8);
        }
        // Desbloquear MFE
        private void button9_Click(object sender, EventArgs e)
        {
            // Criando Número de Sessão.
            numeroSessao = numeroAleatorio.Next(000000, 999999);
            // Criando Identificador.
            identificador = numeroAleatorio.Next(0000000, 9999999);
            // Código de Ativação.
            codigoAtivacao = Convert.ToInt32(textBox1.Text);
            String xmlDesbloquearMfe = "<Integrador><Identificador><Valor>" + identificador + "</Valor>" +
                "</Identificador><Componente Nome=\"MF-e\"><Metodo Nome=\"DesbloquearSAT<Parametros>" +
                "<Parametro><Nome>numeroSessao</Nome><Valor>" + numeroSessao + "</Valor></Parametro><Parametro>" +
                "<Nome>codigoDeAtivacao</Nome><Valor>" + codigoAtivacao + "</Valor></Parametro></Parametros>" +
                "</Metodo></Componente></Integrador>";
            Write(xmlDesbloquearMfe);
            Read(9);
        }
        // Atualizar MFE
        private void button10_Click(object sender, EventArgs e)
        {
            // Criando Número de Sessão.
            numeroSessao = numeroAleatorio.Next(000000, 999999);
            // Criando Identificador.
            identificador = numeroAleatorio.Next(0000000, 9999999);
            // Código de Ativação.
            codigoAtivacao = Convert.ToInt32(textBox1.Text);
            String xmlAtualizarMfe = "<Integrador><Identificador><Valor>" + identificador + "</Valor>" +
                "</Identificador><Componente Nome=\"MF-e\"><Metodo Nome=\"AtualizarSoftwareSAT\"><Parametros>" +
                "<Parametro><Nome>numeroSessao</Nome><Valor>" + numeroSessao + "</Valor></Parametro><Parametro>" +
                "<Nome>codigoDeAtivacao</Nome><Valor>" + codigoAtivacao + "</Valor></Parametro></Parametros> " +
                "</Metodo></Componente></Integrador>";
            Write(xmlAtualizarMfe);
            Read(10);
        }
        // Teste Fim a Fim.
        private void button11_Click(object sender, EventArgs e)
        {
            // Criando Número de Sessão.
            numeroSessao = numeroAleatorio.Next(000000, 999999);
            // Criando Identificador.
            identificador = numeroAleatorio.Next(0000000, 9999999);
            // Código de Ativação.
            codigoAtivacao = Convert.ToInt32(textBox1.Text);
            // CNPJ Contribuinte.
            cnpjContribuinte = textBox2.Text;
            // CNPJ Software House.
            cnpjSoftwareHouse = textBox3.Text;
            // Assinatura da SH.
            assinaturaSoftwareHouse = textBox4.Text;
            // Inscrição Estadual.
            inscricaoEstadual = Convert.ToInt32(textBox8.Text);

            String xmlFimaFim = "<Integrador><Identificador><Valor>"+identificador+"</Valor></Identificador><Componente Nome=\"MF-e\">" +
                "<Metodo Nome=\"TesteFimAFim\"><Parametros><Parametro><Nome>numeroSessao</Nome><Valor>"+numeroSessao+"</Valor></Parametro><Parametro>" +
                "<Nome>codigoDeAtivacao</Nome><Valor>"+codigoAtivacao+"</Valor></Parametro><Parametro><Nome>dadosVenda</Nome><Valor>" +
                "<![CDATA[<CFe><infCFe versaoDadosEnt=\"0.07\"><ide><CNPJ>"+cnpjSoftwareHouse+"</CNPJ><signAC>"+assinaturaSoftwareHouse+"</signAC>" +
                "<numeroCaixa>001</numeroCaixa></ide><emit><CNPJ>"+cnpjContribuinte+"</CNPJ><IE>"+inscricaoEstadual+"</IE><IM>1</IM><indRatISSQN>N</indRatISSQN>" +
                "</emit><dest><CNPJ>15264952000190</CNPJ><xNome>RENATO SILVA</xNome></dest><entrega><xLgr>Ru. Amelia Benebien</xLgr><nro>10</nro>" +
                "<xBairro>Papicu</xBairro><xMun>Fortaleza</xMun><UF>CE</UF></entrega><det nItem=\"1\"><prod><cProd>000010</cProd>" +
                "<xProd>BOLA FUTEBOL</xProd><NCM>00000000</NCM><CFOP>5102</CFOP><uCom>UN</uCom><qCom>10.0000</qCom><vUnCom>14.000</vUnCom><indRegra>A</indRegra>" +
                "</prod><imposto><vItem12741>4.17</vItem12741><ICMS><ICMS00><Orig>0</Orig><CST>00</CST><pICMS>17.00</pICMS></ICMS00></ICMS><PIS><PISAliq>" +
                "<CST>01</CST><vBC>10.00</vBC><pPIS>5.0000</pPIS></PISAliq></PIS><COFINS><COFINSAliq><CST>01</CST><vBC>20.00</vBC><pCOFINS>0.5000</pCOFINS>" +
                "</COFINSAliq></COFINS></imposto></det><total><vCFeLei12741>28.91</vCFeLei12741></total><pgto><MP><cMP>01</cMP><vMP>100.00</vMP></MP><MP><cMP>02</cMP>" +
                "<vMP>13.21</vMP></MP><MP><cMP>03</cMP><vMP>30.00</vMP></MP></pgto><infAdic></infAdic></infCFe></CFe>]]></Valor></Parametro></Parametros>" +
                "</Metodo</Componente></Integrador>";

            Write(xmlFimaFim);
            Read(11);
        }
        // Enviar Dados Venda
        private void button12_Click(object sender, EventArgs e)
        {
            // Criando Número de Sessão.
            numeroSessao = numeroAleatorio.Next(000000, 999999);
            // Criando Identificador.
            identificador = numeroAleatorio.Next(0000000, 9999999);
            // Código de Ativação.
            codigoAtivacao = Convert.ToInt32(textBox1.Text);
            // CNPJ Contribuinte.
            cnpjContribuinte = textBox2.Text;
            // CNPJ Software House.
            cnpjSoftwareHouse = textBox3.Text;
            // Assinatura da SH.
            assinaturaSoftwareHouse = textBox4.Text;
            // Inscrição Estadual.
            inscricaoEstadual = Convert.ToInt32(textBox8.Text);
            String xmlEnviarVenda = "<?xml version=\"1.0\"?><Integrador><Identificador>" +
                "<Valor>"+identificador+"</Valor></Identificador><Componente Nome=\"MF-e\">" +
                "<Metodo Nome=\"EnviarDadosVenda\"><Parametros><Parametro><Nome>numeroSessao</Nome>" +
                "<Valor>"+numeroSessao+"</Valor></Parametro><Parametro><Nome>codigoDeAtivacao</Nome><Valor>"+codigoAtivacao+"</Valor>" +
                "</Parametro><Parametro><Nome>dadosVenda</Nome><Valor><![CDATA[<CFe><infCFe versaoDadosEnt=\"0.07\">" +
                "<ide><CNPJ>"+cnpjSoftwareHouse+"</CNPJ><signAC>"+assinaturaSoftwareHouse+"</signAC><numeroCaixa>001</numeroCaixa>" +
                "</ide><emit><CNPJ>"+cnpjContribuinte+"</CNPJ><IE>"+inscricaoEstadual+"</IE><indRatISSQN>N</indRatISSQN></emit><dest/>" +
                "<det nItem=\"1\"><prod><cProd>1</cProd><cEAN>0012345678905</cEAN><xProd>NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM " +
                "VALOR FISCAL</xProd><NCM>30043929</NCM><CFOP>5102</CFOP><uCom>UN</uCom><qCom>1.0000</qCom><vUnCom>4.40</vUnCom>" +
                "<indRegra>A</indRegra></prod><imposto><vItem12741>2.00</vItem12741><ICMS><ICMSSN102><Orig>0</Orig><CSOSN>102</CSOSN>" +
                "</ICMSSN102></ICMS><PIS><PISAliq><CST>01</CST><vBC>4.40</vBC><pPIS>0.0165</pPIS></PISAliq></PIS><COFINS><COFINSAliq>" +
                "<CST>01</CST><vBC>4.40</vBC><pCOFINS>0.0760</pCOFINS></COFINSAliq></COFINS></imposto></det><total><vCFeLei12741>2.00" +
                "</vCFeLei12741></total><pgto><MP><cMP>04</cMP><vMP>10.00</vMP></MP></pgto><infAdic><infCpl>Nao Aceitamos Devolucoes sem " +
                "este Comprovante</infCpl></infAdic></infCFe></CFe>]]></Valor></Parametro><Parametro><Nome>nrDocumento</Nome>" +
                "<Valor>12345678</Valor></Parametro></Parametros></Metodo></Componente></Integrador>";
            Write(xmlEnviarVenda);
            Read(12);
        }
        // Cancelar Venda
        private void button13_Click(object sender, EventArgs e)
        {
        }


        // Método de Escrita.
        public void Write(String xml)
        {
            int xmlDeEntrada = numeroAleatorio.Next(0000000, 9999999);
            String nomexml = Convert.ToString("\\" + xmlDeEntrada + ".xml");
            // Salvando XML na Input.
            File.WriteAllText(textBox6.Text + nomexml, xml);
        }
        // Método de Leitura.
        public void Read(int tipo)
        {
            switch (tipo)
            {
                case 1: Thread.Sleep(1500);
                    break;
                case 2: Thread.Sleep(2000);
                    break;
                case 3: Thread.Sleep(2000);
                    break;
                case 4: 
                    break;
                case 5: Thread.Sleep(20000);
                    break;
                case 6: Thread.Sleep(20000);
                    break;
                case 7: Thread.Sleep(1500);
                    break;
                case 8: Thread.Sleep(5000);
                    break;
                case 9: Thread.Sleep(5000);
                    break;
                case 10: Thread.Sleep(60000);
                    break;
                case 11: Thread.Sleep(10000);
                    break;
                case 12: Thread.Sleep(3000);
                    break;
            }
            // Caminho do Diretório de Arquivos Processados.
            String movee = @"C:\ArquivosProcessados\";
            // Listando Arquivos na Output.
            String[] arquivos = Directory.GetFiles(textBox7.Text);
            while (arquivos[i] != null)
            {
                // Split para pegar o nome do XML.
                String[] deliminador = arquivos[i].Split('\\');
                String nome = deliminador[3];
                // Instancia do XmlDocument.
                XmlDocument xmldoc = new XmlDocument();
                // Abrindo xml.
                xmldoc.Load(arquivos[i]);
                // Pega o Nó Global.
                XmlNode integrador = xmldoc.SelectSingleNode("Integrador");
                // Pega o Nó Identificador.
                XmlNode globalNode = integrador.SelectSingleNode("Identificador");
                // Pega o Nó Valor.
                XmlNode valor = globalNode.SelectSingleNode("Valor");
                // Comparando Identificador do XML de retorno com o Identificador do XML de entrada.
                if (valor.InnerText == Convert.ToString(identificador))
                {
                    integrador = xmldoc.SelectSingleNode("Integrador");
                    globalNode = integrador.SelectSingleNode("Resposta");
                    valor = globalNode.SelectSingleNode("retorno");
                    String retorno = valor.InnerText;
                    String[] separator = retorno.Split('|');
                    switch (tipo)
                    {
                        case 1:
                            textBox5.Text = separator[1] + " - " + separator[2];
                            break;
                        case 2:
                            textBox5.Text = "Número de Sessão: " + Environment.NewLine +
                                separator[0] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Código EEEEE: " + Environment.NewLine + 
                                separator[1] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Mensagem EEEEE: " + Environment.NewLine + 
                                separator[2] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Código de Referência: " + Environment.NewLine + 
                                separator[3] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Mensagem SEFAZ: " + Environment.NewLine + 
                                separator[4] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Número de Série: " + Environment.NewLine + 
                                separator[5] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Tipo de Lan: " + Environment.NewLine + 
                                separator[6] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "IP da Lan: " + Environment.NewLine + 
                                separator[7] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "MAC da Lan: " + Environment.NewLine + 
                                separator[8] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Mascara Sub-Rede: " + Environment.NewLine + 
                                separator[9] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Endereço de Gateway: " + Environment.NewLine + 
                                separator[10] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Endereço de DNS1: " + Environment.NewLine + 
                                separator[11] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Endereço de DNS2: " + Environment.NewLine + 
                                separator[12] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Status da Rede: " + Environment.NewLine + 
                                separator[13] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Nível de Bateria: " + Environment.NewLine + 
                                separator[14] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Memória Total: " + Environment.NewLine + 
                                separator[15] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Memória Usada: " + Environment.NewLine + 
                                separator[16] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Data e Hora: " + Environment.NewLine + 
                                separator[17] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Versão Software Básico: " + Environment.NewLine + 
                                separator[18] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Versão Layout: " + Environment.NewLine + 
                                separator[19] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Ultimo CFe Emitido: " + Environment.NewLine + 
                                separator[20] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Primeiro CFe Armazenado: " + Environment.NewLine + 
                                separator[21] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Último CFe Armazenado: " + Environment.NewLine + 
                                separator[22] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Última Transmissão: " + Environment.NewLine + 
                                separator[23] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Última Comunicação: " + Environment.NewLine + 
                                separator[24] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Emissão do Certificado: " + Environment.NewLine + 
                                separator[25] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Vencimento do Certificado: " + Environment.NewLine + 
                                separator[26] + Environment.NewLine + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Estado de Operação: " + Environment.NewLine + 
                                separator[27] + Environment.NewLine + Environment.NewLine;
                            break;
                        case 3:
                            byte[] byt = Convert.FromBase64String(separator[5]);
                            String decodeBase64 = Encoding.UTF8.GetString(byt);
                            File.WriteAllText(movee + "LOGEXTRAIDO", decodeBase64);
                            textBox5.Text = separator[1] + " - " + separator[2] + Environment.NewLine;
                            textBox5.Text = textBox5.Text + "Seu LOG foi salvo na pasta ArquivosProcessados no C:";
                            break;
                        case 4:

                            break;
                        case 5:
                            textBox5.Text = separator[1] + " - " + separator[2];
                            break;
                        case 6:
                            textBox5.Text = separator[1] + " - " + separator[2];
                            break;
                        case 7:
                            textBox5.Text = separator[1] + " - " + separator[2];
                            break;
                        case 8:
                            textBox5.Text = separator[1] + " - " + separator[2];
                            break;
                        case 9:
                            textBox5.Text = separator[1] + " - " + separator[2];
                            break;
                        case 10:
                            textBox5.Text = separator[1] + " - " + separator[2];
                            break;
                        case 11:
                            textBox5.Text = separator[1] + " - " + separator[2];
                            break;
                        case 12:
                            textBox5.Text = separator[1] + " - " + separator[3] + Environment.NewLine;
                            textBox5.Text = textBox5.Text + Environment.NewLine + "O retorno da venda foi salvo na pasta ArquivosProcessados no C:";
                            byte[] bytt = Convert.FromBase64String(separator[6]);
                            decodeBase64 = Encoding.UTF8.GetString(bytt);
                            File.WriteAllText(movee + "VENDAEFETUADA", decodeBase64);
                            break;
                    }
                    if (File.Exists(movee))
                    {
                        File.Move(arquivos[i], movee + nome);
                    }
                    else
                    {
                        Directory.CreateDirectory(movee);
                        File.Move(arquivos[i], movee + nome);
                    }
                    break;
                }
                else
                {
                    i++;
                }
            }
        }
    }
}